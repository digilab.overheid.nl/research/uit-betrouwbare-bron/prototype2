use std::{
    fmt::{Display, Formatter},
    sync::{Arc, Mutex},
};

use crate::{
    PrimitiveType, PRIMITIVE_TYPE_BOOLEAN, PRIMITIVE_TYPE_DATE, PRIMITIVE_TYPE_DATETIME,
    PRIMITIVE_TYPE_DECIMAL, PRIMITIVE_TYPE_INTEGER, PRIMITIVE_TYPE_STRING, PRIMITIVE_TYPE_UUID,
};

#[derive(Debug)]
pub struct InnerEngineConfiguration {
    pub(crate) primitive_types: Mutex<Vec<PrimitiveType>>, // Settings: Key / Value map
}

#[derive(Default)]
pub struct EngineConfigurationBuilder {
    primitive_types: Vec<PrimitiveType>,
}

impl EngineConfigurationBuilder {
    pub fn add_primitive_type(mut self, primitive_type: PrimitiveType) -> Self {
        self.primitive_types.push(primitive_type);
        self
    }

    pub fn add_default_primitives(self) -> Self {
        self.add_primitive_type(PRIMITIVE_TYPE_UUID.clone())
            .add_primitive_type(PRIMITIVE_TYPE_INTEGER.clone())
            .add_primitive_type(PRIMITIVE_TYPE_STRING.clone())
            .add_primitive_type(PRIMITIVE_TYPE_BOOLEAN.clone())
            .add_primitive_type(PRIMITIVE_TYPE_DATE.clone())
            .add_primitive_type(PRIMITIVE_TYPE_DATETIME.clone())
            .add_primitive_type(PRIMITIVE_TYPE_DECIMAL.clone())
    }

    pub fn build(self) -> EngineConfiguration {
        EngineConfiguration::new(self.primitive_types)
    }
}

#[derive(Clone, Debug)]
pub struct EngineConfiguration {
    pub(crate) inner: Arc<InnerEngineConfiguration>,
}

impl EngineConfiguration {
    fn new(primitive_types: Vec<PrimitiveType>) -> Self {
        EngineConfiguration {
            inner: Arc::new(InnerEngineConfiguration {
                primitive_types: Mutex::new(primitive_types),
            }),
        }
    }

    pub(crate) fn inner(&self) -> &Arc<InnerEngineConfiguration> {
        &self.inner
    }

    pub fn add_primitive_type(&self, primative_type: PrimitiveType) -> Result<(), std::io::Error>
where {
        let mut primitive_type_lock = self
            .inner()
            .primitive_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        primitive_type_lock.push(primative_type);

        Ok(())
    }

    pub fn get_primitive_type<T>(&self, name: T) -> Result<PrimitiveType, std::io::Error>
    where
        T: AsRef<str>,
    {
        let primitive_types_lock = self
            .inner()
            .primitive_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        let res = primitive_types_lock
            .iter()
            .find(|v| v.name() == name.as_ref())
            .ok_or_else(|| {
                std::io::Error::new(
                    std::io::ErrorKind::NotFound,
                    format!("Primitive Type {} not found", name.as_ref()),
                )
            })?;

        Ok(res.clone())
    }

    pub(crate) fn _find_primitive_type<T>(
        &self,
        name: T,
    ) -> Result<Option<PrimitiveType>, std::io::Error>
    where
        T: AsRef<str>,
    {
        let primitive_types_lock = self
            .inner()
            .primitive_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        let res = primitive_types_lock
            .iter()
            .find(|v| v.name() == name.as_ref())
            .cloned();

        Ok(res)
    }
}

impl Display for EngineConfiguration {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        let primitive_types_lock = self.inner().primitive_types.lock();

        let lock = match primitive_types_lock {
            Ok(lock) => lock,
            Err(e) => {
                writeln!(f, "Failed to lock engine for formatting?s: {}", e)?;
                return Ok(());
            }
        };

        write!(f, "Configured Primitive Types: ")?;
        for primitive_type in lock.iter() {
            write!(f, "\n\t Primitive Type: ({})", primitive_type)?;
        }
        writeln!(f)?;

        Ok(())
    }
}
