use crate::{
    EngineConfiguration, PrimitiveType, Schema, SchemaManager, PRIMITIVE_TYPE_INTEGER,
    PRIMITIVE_TYPE_STRING, PRIMITIVE_TYPE_UUID,
};

pub fn bootstrap(
    engine_config: &EngineConfiguration,
    schema_manager: &SchemaManager,
    schema_name: &str,
) -> Result<Schema, std::io::Error> {
    let schema = Schema::new(engine_config, schema_name)?;

    schema_manager.register_schema(schema.clone());

    let must_should_enum = PrimitiveType::new("enum_must_should", |value| {
        value == "must" || value == "should"
    });

    engine_config.add_primitive_type(must_should_enum.clone())?;

    schema.add_label_type("id", &PRIMITIVE_TYPE_UUID)?;
    schema.add_label_type("name", &PRIMITIVE_TYPE_STRING)?;
    schema.add_label_type("expression", &PRIMITIVE_TYPE_STRING)?;
    schema.add_label_type("sequence number", &PRIMITIVE_TYPE_INTEGER)?;
    schema.add_label_type("must/should", &must_should_enum)?;
    // enum: must, should
    // Zou mooi zijn als we een waardelijst bij een labeltype zouden kunnen opgeven.
    // Kan wat mij betreft in eerste instantie gewoon als strings die naar het primitieve type vertaald kunnen worden
    // (of je moet direct al een vector van de primitive types willen maken en de conversie bij de creatie doen, is natuurlijk wel veel veiliger)

    // -- Primitive type

    schema.add_claim_type("Primitive type", "Primitive type <name> exists")?;
    schema.add_claim_type_ote("Primitive type", "primitive type <name>")?;
    schema.add_claim_type_unicity("Primitive type", vec!["name"])?;

    // -- Schema

    schema.add_claim_type("Schema", "Schema <name> exists")?;
    schema.add_claim_type_ote("Schema", "schema <name>")?;
    schema.add_claim_type_unicity("Schema", vec!["name"])?;

    // // -- Label type

    schema.add_claim_type("Label type", "Label type <id> exists")?;
    schema.add_claim_type_ote("Label type", "label type <id>")?;
    schema.add_claim_type_unicity("Label type", vec!["id"])?;

    schema.add_claim_type("Label type / name", "<Label type> is called <name>")?;
    schema.add_claim_type_unicity("Label type / name", vec!["Label type"])?;

    schema.add_claim_type(
        "Label type / primitive",
        "<Label type> is implemented as <Primitive type>",
    )?;
    schema.add_claim_type_unicity("Label type / primitive", vec!["Label type"])?;

    schema.add_claim_type("Schema / Label type", "<Schema> contains <Label type>")?;
    schema.add_claim_type_unicity("Schema / Label type", vec!["Label type"])?;

    // // -- Claim type

    schema.add_claim_type("Claim type", "Claim type <id> exists")?;
    schema.add_claim_type_ote("Claim type", "claim type <id>")?;
    schema.add_claim_type_unicity("Claim type", vec!["id"])?;

    schema.add_claim_type("Claim type / name", "<Claim type> is called <name>")?;
    schema.add_claim_type_unicity("Claim type / name", vec!["Claim type"])?;

    schema.add_claim_type(
        "Claim type / expression",
        "<Claim type> can be verbalised by \"<expression>\"",
    )?;
    schema.add_claim_type_unicity("Claim type / expression", vec!["Claim type"])?;

    schema.add_claim_type("Claim type / nested expression", "<Claim type> can be verbalised as \"<nested expression:expression>\" when substituted in other expressions")?;
    schema.add_claim_type_unicity("Claim type / nested expression", vec!["Claim type"])?;

    schema.add_claim_type("Schema / Claim type", "<Schema> contains <Claim type>")?;
    schema.add_claim_type_unicity("Schema / Claim type", vec!["Claim type"])?;

    // // -- Role

    schema.add_claim_type("Role", "Role <id> exists")?;
    schema.add_claim_type_ote("Role", "role <id>")?;

    schema.add_claim_type("Role / name", "<Role> is called <name>")?;
    schema.add_claim_type_unicity("Role / name", vec!["Role"])?;

    schema.add_claim_type("Role / part of", "<Role> is part of <part of:Claim type>")?;
    // TODO: Make use off `Claim type` in stead of `part of` possible
    schema.add_claim_type_unicity("Role / part of", vec!["part of", "Role"])?;
    // // unicity: Claim type, Role

    schema.add_claim_type(
        "Role / played by claim type",
        "<Role> is played by <played by claim type:Claim type>",
    )?;
    schema.add_claim_type_unicity("Role / played by claim type", vec!["Role"])?;

    schema.add_claim_type(
        "Role / played by label type",
        "<Role> is played by <played by label type:Label type>",
    )?;
    schema.add_claim_type_unicity("Role / played by label type", vec!["Role"])?;

    // // -- Discuss? Alternative: Introduce subtype that combines Label type and Claim type

    // // -- Verbalisation 'borrowed' from T. Halpin
    schema.add_claim_type("Role / Totality", "Every instance in the population of the type of <Role> <population total:must/should> play that role")?;
    schema.add_claim_type_unicity("Role / Totality", vec!["Role"])?;

    // // -- Unicity
    // // --- Ugly, sets are not supported, so we have to introduce an artificial id

    schema.add_claim_type("Unicity", "Unicity <id> exists")?;
    schema.add_claim_type_ote("Unicity", "unicity <id>")?;
    schema.add_claim_type_unicity("Unicity", vec!["id"])?;

    schema.add_claim_type(
        "Unicity / part of",
        "<Claim type> can be uniquely identified by <Unicity>",
    )?;
    schema.add_claim_type_unicity("Unicity / part of", vec!["Unicity"])?;

    schema.add_claim_type(
        "Unicity / Role",
        "<Unicity> contains <Role> at position <sequence number>",
    )?;

    schema.add_claim_type_unicity("Unicity / Role", vec!["Unicity", "Role"])?;
    schema.add_claim_type_unicity("Unicity / Role", vec!["Unicity", "sequence number"])?;

    Ok(schema)
}

#[cfg(test)]
mod bootstrap {
    use crate::EngineConfigurationBuilder;
    use std::fmt::Write as _;

    use super::*;
    use anyhow::Result;

    #[test]
    fn bootstrap_test() -> Result<()> {
        let schema_manager = SchemaManager::default();
        let engine_config = EngineConfigurationBuilder::default()
            .add_default_primitives()
            .build();

        let schema = bootstrap(&engine_config, &schema_manager, "ace")?;

        println!("{:#?}", schema);

        // assert!(false);
        Ok(())
    }

    use std::hash::{DefaultHasher, Hash, Hasher};
    // Prevent schema changing by using hashed name instead of uuid
    fn calculate_hash<T: Hash>(t: &T) -> u64 {
        let mut s = DefaultHasher::new();
        t.hash(&mut s);
        s.finish()
    }

    #[test]
    fn bootstrap_render_test() -> Result<()> {
        // Render the schema as a mermaid flowchart

        let schema_manager = SchemaManager::default();
        let engine_config = EngineConfigurationBuilder::default()
            .add_default_primitives()
            .build();

        let schema = bootstrap(&engine_config, &schema_manager, "ace")?;

        let name = schema.inner().name.clone();
        let primitives = schema
            .inner()
            .engine
            .inner()
            .primitive_types
            .lock()
            .unwrap()
            .clone();

        let labels = schema.inner().label_types.lock().unwrap().clone();
        let claims = schema.inner().claim_types.lock().unwrap().clone();

        let mut data = String::new();

        writeln!(&mut data, "```mermaid")?;
        writeln!(&mut data, "---\ntitle: {} schema\n---", name)?;

        writeln!(&mut data, "flowchart LR")?;
        writeln!(&mut data, "classDef hidden display:none;")?;

        // Primitives
        writeln!(&mut data, "rank_primitive:::hidden")?;

        for primitive in primitives {
            writeln!(&mut data, "{}({})", primitive.name(), primitive.name())?;
            writeln!(&mut data, "{} ~~~ rank_primitive", primitive.name())?;
        }

        // Label types

        writeln!(&mut data, "rank_label:::hidden")?;
        for label in labels {
            let name = label.inner().name.clone();
            let id = calculate_hash(&format!("label:{}", name));
            let primitive_name = label.inner().primitive_type.name();
            writeln!(&mut data, "{}>{}]", id, name)?;
            writeln!(&mut data, "{} ---> {}", id, primitive_name)?;
            writeln!(&mut data, "{} ~~~ rank_label", id)?;
        }

        // Claim types
        //
        writeln!(&mut data, "rank_claim:::hidden")?;
        for claimtype in claims {
            let inner = claimtype.inner().lock().unwrap();
            let name = inner.name.clone();
            let id = calculate_hash(&format!("claim:{}", name));
            writeln!(&mut data, "{}{{{{{}}}}}", id, name)?;
            writeln!(&mut data, "{} ~~~ rank_claim", id)?;

            let roles = inner.roles.clone();
            for role in roles {
                let role_type = role.role_type;

                match role_type {
                    crate::RoleType::LabelType(label) => {
                        let label_inner = label.inner();
                        let label_id = calculate_hash(&format!("label:{}", &label_inner.name));
                        writeln!(&mut data, "{} ---> {}", id, label_id)?;
                    }
                    crate::RoleType::ClaimType(claim) => {
                        let claim_inner = claim.inner().lock().unwrap();
                        let claim_id = calculate_hash(&format!("claim:{}", &claim_inner.name));
                        writeln!(&mut data, "{} ----> {}", id, claim_id)?;
                    }
                };
            }
        }

        writeln!(&mut data, "```")?;

        println!("{}", data);
        std::fs::write("docs/bootstrap_schema.md", data).expect("Unable to write file");
        Ok(())
    }
}
