mod bootstrap;
mod engine_configuration;
mod parse;
mod schema;
mod schema_manager;
mod types;

pub use engine_configuration::*;
pub use schema::*;
pub use schema_manager::*;
pub use types::*;

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod test {
    use super::*;
    use anyhow::Result;

    #[test]
    fn test_engine() -> Result<()> {
        let engine_config = EngineConfigurationBuilder::default()
            .add_default_primitives()
            .build();

        let schema = Schema::new(&engine_config, "default")?;

        schema.add_label_type("id", &PRIMITIVE_TYPE_UUID)?;
        schema.add_label_type("voornaam", &PRIMITIVE_TYPE_STRING)?;

        let unknown_primitive = PrimitiveType::new("unknown_primitive", |_value| true);

        let res = schema.add_label_type("lengte", &unknown_primitive);
        assert!(res.is_err());

        engine_config.add_primitive_type(unknown_primitive.clone())?;
        let res = schema.add_label_type("lengte", &unknown_primitive);
        assert!(res.is_ok());

        println!("{}", schema);
        Ok(())
    }

    #[tokio::test]
    async fn test_acync() -> Result<()> {
        let engine_config = EngineConfigurationBuilder::default()
            .add_default_primitives()
            .build();
        let schema_manager = SchemaManager::default();

        let schema1 = Schema::new(&engine_config, "s1")?;
        schema_manager.register_schema(schema1.clone());
        let schema2 = Schema::new(&engine_config, "s2")?;
        schema_manager.register_schema(schema2.clone());

        let unknown_primitive = PrimitiveType::new("unknown_primitive", |_value| true);

        async fn add_primitive_type(
            engine: &EngineConfiguration,
            primitive_type: PrimitiveType,
        ) -> Result<()> {
            engine.add_primitive_type(primitive_type)?;
            Ok(())
        }

        async fn add_label(schema: &Schema, primitive_type: &PrimitiveType) -> Result<()> {
            schema.add_label_type("lengte", primitive_type)?;
            Ok(())
        }

        add_primitive_type(&engine_config, unknown_primitive.clone()).await?;

        add_label(&schema1, &unknown_primitive).await?;
        add_label(&schema2, &unknown_primitive).await?;

        println!("{}", schema_manager);

        Ok(())
    }
}
