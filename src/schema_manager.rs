use std::{
    fmt::Display,
    sync::{Arc, Mutex, Weak},
};

use crate::{InnerSchema, Schema};

#[derive(Default)]
pub struct SchemaManager {
    pub schemas: Mutex<Vec<Weak<InnerSchema>>>,
}

impl SchemaManager {
    pub fn register_schema(&self, schema: Schema) {
        self.schemas
            .lock()
            .unwrap()
            .push(Arc::downgrade(schema.inner()));
    }
}

impl Display for SchemaManager {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for schema in self.schemas.lock().unwrap().iter() {
            writeln!(f, "{}", schema.upgrade().unwrap())?;
        }

        Ok(())
    }
}
