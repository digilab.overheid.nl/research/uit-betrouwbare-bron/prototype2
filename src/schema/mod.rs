use std::{
    fmt::{Display, Formatter},
    sync::{Arc, Mutex},
};

use crate::{
    parse::Expression,
    types::{ClaimType, Role},
    ClaimTypeBuilder, EngineConfiguration, LabelType, PrimitiveType, RoleType,
};

#[derive(Debug)]
pub struct InnerSchema {
    pub(crate) name: String,
    pub(crate) label_types: Mutex<Vec<Arc<LabelType>>>,
    pub(crate) claim_types: Mutex<Vec<Arc<ClaimType>>>,
    pub(crate) engine: EngineConfiguration,
}

#[derive(Clone, Debug)]
pub struct Schema {
    inner: Arc<InnerSchema>,
}

impl Schema {
    pub fn new<T>(engine: &EngineConfiguration, name: T) -> Result<Self, std::io::Error>
    where
        T: Into<String>,
    {
        let schema = Schema {
            inner: Arc::new(InnerSchema {
                name: name.into(),
                label_types: Default::default(),
                claim_types: Default::default(),
                engine: engine.clone(),
            }),
        };

        Ok(schema)
    }

    pub(crate) fn inner(&self) -> &Arc<InnerSchema> {
        &self.inner
    }

    pub fn add_label_type<T>(
        &self,
        name: T,
        primitive_type: &PrimitiveType,
    ) -> Result<(), std::io::Error>
    where
        T: Into<String>,
    {
        // Check if primitive type is known
        let primitive_types = self
            .inner()
            .engine
            .inner()
            .primitive_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        let primitive_type = primitive_types
            .iter()
            .find(|pt| pt.name() == primitive_type.name())
            .ok_or_else(|| {
                std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Unknown primitive type: {}", primitive_type.name()),
                )
            })?;

        let label_type = LabelType::new(name, primitive_type.clone());
        let arc = Arc::new(label_type);

        let mut labeltypes_lock = self
            .inner()
            .label_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        labeltypes_lock.push(arc);

        Ok(())
    }

    pub fn find_label_type(&self, name: &str) -> Result<Option<Arc<LabelType>>, std::io::Error> {
        let labeltypes_lock = self
            .inner()
            .label_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        Ok(labeltypes_lock
            .iter()
            .find(|lt| lt.inner().name == name)
            .cloned())
    }

    fn get_roles_from_expression(
        &self,
        expression: Expression,
    ) -> Result<Vec<Role>, std::io::Error> {
        let mut roles = vec![];

        for part in expression.clone().parts {
            if let crate::parse::ExpressionPart::Role(role) = part {
                let role_name = role.role_name;
                let role_type = role.role_type;

                let id = role_type.unwrap_or(role_name.clone());

                let label_role = self.find_label_type(&id.inner())?;
                let claim_role = self.find_claim_type(&id.inner())?;

                if let Some(label_role) = label_role {
                    roles.push(Role {
                        role_name: role_name.inner(),
                        role_type: RoleType::LabelType(label_role),
                    });
                } else if let Some(claim_role) = claim_role {
                    roles.push(Role {
                        role_name: role_name.inner(),
                        role_type: RoleType::ClaimType(claim_role),
                    });
                } else {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        format!("Unknown role: {}", id.inner()),
                    ));
                }
            }
        }

        Ok(roles)
    }

    pub fn add_claim_type<T, U>(
        &self,
        name: T,
        claim_type_expression: U,
    ) -> Result<(), std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let parsed_expresssion = crate::parse::parse_expression(claim_type_expression.as_ref())?;
        let roles = self.get_roles_from_expression(parsed_expresssion)?;

        let builder = ClaimTypeBuilder::new(
            name.as_ref().to_string(),
            claim_type_expression.as_ref().to_string(),
            roles,
        );

        let claim_type = builder.build();

        let arc = Arc::new(claim_type);

        let mut claimtypes_lock = self
            .inner()
            .claim_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        claimtypes_lock.push(arc);

        Ok(())
    }

    pub fn add_claim_type_unicity<T, U>(
        &self,
        name: T,
        role_names: Vec<U>,
    ) -> Result<(), std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let claim_type = self.find_claim_type(name.as_ref())?;

        let claim_type: Arc<ClaimType> = match claim_type {
            Some(claim_type) => claim_type,
            None => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Unknown claim type: {}", name.as_ref()),
                ));
            }
        };

        let mut claim_type_lock = claim_type.inner().lock().unwrap();
        claim_type_lock.add_unicity(role_names);
        Ok(())
    }

    pub fn add_claim_type_ote<T, U>(
        &self,
        name: T,
        object_type_expression: U,
    ) -> Result<(), std::io::Error>
    where
        T: AsRef<str>,
        U: AsRef<str>,
    {
        let parsed_expresssion = crate::parse::parse_expression(object_type_expression.as_ref())?;

        let mut role_names = vec![];

        for part in parsed_expresssion.parts {
            if let crate::parse::ExpressionPart::Role(role) = part {
                role_names.push(role.role_name.inner());
            }
        }

        let claim_type = self.find_claim_type(name.as_ref())?;

        let claim_type: Arc<ClaimType> = match claim_type {
            Some(claim_type) => claim_type,
            None => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Unknown claim type: {}", name.as_ref()),
                ));
            }
        };

        let mut claim_type_lock = claim_type.inner().lock().unwrap();
        claim_type_lock.add_ote(object_type_expression.as_ref().to_string(), role_names);
        Ok(())
    }

    pub fn find_claim_type(&self, name: &str) -> Result<Option<Arc<ClaimType>>, std::io::Error> {
        let claimtypes_lock = self
            .inner()
            .claim_types
            .lock()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;

        Ok(claimtypes_lock.iter().find(|lt| lt.name == name).cloned())
    }
}

impl Display for Schema {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.inner())
    }
}

impl Display for InnerSchema {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        writeln!(f, "Schema: `{}`", self.name)?;
        write!(f, "Label types:")?;
        {
            let labeltypes_lock = self.label_types.lock();
            let labeltypes_lock = match labeltypes_lock {
                Ok(lock) => lock,
                Err(e) => {
                    writeln!(f, "Failed to lock schema for formatting?s: {}", e)?;
                    return Ok(());
                }
            };
            for label_type in labeltypes_lock.iter() {
                write!(f, "\n\t Label: ({})", label_type)?;
            }
        }

        write!(f, "Claim types:")?;
        {
            let claimtypes_lock = self.claim_types.lock();
            let claimtypes_lock = match claimtypes_lock {
                Ok(lock) => lock,
                Err(e) => {
                    writeln!(f, "Failed to lock schema for formatting?s: {}", e)?;
                    return Ok(());
                }
            };
            for claim_type in claimtypes_lock.iter() {
                write!(f, "\n\t ClaimType: ({})", claim_type)?;
            }
        }

        writeln!(f)?;
        write!(f, "{}", self.engine)?;

        Ok(())
    }
}
