use std::{
    fmt::{Display, Formatter},
    sync::{Arc, Mutex},
};

use uuid::Uuid;

use crate::LabelType;

pub struct ClaimTypeBuilder {
    pub name: String,
    pub claim_type_expression: String, // claim type expression
    pub object_type_expression: Option<String>, // object type expression (substitution expression)
    pub roles: Vec<Role>,
    pub unicities: Vec<Unicity>,
}

impl ClaimTypeBuilder {
    pub fn new(name: String, claim_type_expression: String, roles: Vec<Role>) -> Self {
        Self {
            name,
            claim_type_expression,
            object_type_expression: None,
            roles,
            unicities: Vec::new(),
        }
    }

    pub fn build(&self) -> ClaimType {
        ClaimType {
            name: self.name.clone(),
            inner: Arc::new(Mutex::new(InnerClaimType {
                id: Uuid::new_v4(),
                name: self.name.clone(),
                claim_type_expression: self.claim_type_expression.clone(),
                object_type_expression: self.object_type_expression.clone(),
                roles: self.roles.clone(),
                unicities: self.unicities.clone(),
            })),
        }
    }
}

#[derive(Clone, Debug)]
pub struct ClaimType {
    pub name: String,
    pub(crate) inner: Arc<Mutex<InnerClaimType>>,
}

impl ClaimType {
    pub fn inner(&self) -> &Arc<Mutex<InnerClaimType>> {
        &self.inner
    }
}

impl Display for ClaimType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let claim_type_lock = self.inner.lock().unwrap();
        write!(f, "{}", claim_type_lock)
    }
}

#[derive(Clone, Debug)]
pub struct InnerClaimType {
    pub id: Uuid,
    pub name: String,
    pub claim_type_expression: String, // claim type expression
    pub object_type_expression: Option<String>, // object type expression (substitution expression)
    pub roles: Vec<Role>,
    pub unicities: Vec<Unicity>,
}

impl InnerClaimType {
    pub fn add_unicity<U>(&mut self, role_names: Vec<U>) -> &mut Self
    where
        U: AsRef<str>,
    {
        let mut unicity_roles: Vec<Role> = vec![];

        for role_name in role_names.iter() {
            let claim_role = self
                .roles
                .iter()
                .find(|v| v.role_name == role_name.as_ref());
            let unicity_role = unicity_roles
                .iter()
                .find(|v| v.role_name == role_name.as_ref());

            if unicity_role.is_some() {
                panic!("Role already added to unicity");
            }

            if let Some(role) = claim_role {
                unicity_roles.push(role.clone());
            } else {
                panic!(
                    "Role `{}` not found in claim type `{}` roles",
                    role_name.as_ref(),
                    self.name
                );
            }
        }

        self.unicities.push(Unicity {
            roles: unicity_roles,
        });
        self
    }

    pub fn add_ote(
        &mut self,
        object_type_expression: String,
        role_names: Vec<String>,
    ) -> &mut Self {
        let mut ote_roles: Vec<Role> = vec![];

        if role_names.len() != self.roles.len() {
            panic!("Number of roles in object type expression does not match number of roles in claim type expression");
        }

        for role_name in role_names.iter() {
            let claim_role = self.roles.iter().find(|v| v.role_name == *role_name);
            let unicity_role = ote_roles.iter().find(|v| v.role_name == *role_name);

            if unicity_role.is_some() {
                panic!("Role already added to unicity");
            }

            if let Some(role) = claim_role {
                ote_roles.push(role.clone());
            } else {
                panic!("Role not found in roles");
            }
        }

        self.object_type_expression = Some(object_type_expression);

        self
    }
}

impl Display for InnerClaimType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "ClaimType: \n\tname: `{}` \n\tcte: `{}` \n\tote: `{:?}` \n\troles: `{:?}` \n\nunicities: `{:#?}`\n",
            self.name,
            self.claim_type_expression,
            self.object_type_expression,
            self.roles,
            self.unicities
        )
    }
}

#[derive(Clone, Debug)]
pub enum RoleType {
    LabelType(Arc<LabelType>),
    ClaimType(Arc<ClaimType>),
}

#[derive(Clone, Debug)]
pub struct Role {
    pub role_name: String,
    pub role_type: RoleType,
}

impl PartialEq for Role {
    fn eq(&self, other: &Self) -> bool {
        self.role_name == other.role_name
    }
}

#[derive(Clone, Debug)]
pub struct Unicity {
    pub roles: Vec<Role>,
}

#[cfg(test)]
mod test {
    use crate::{PRIMITIVE_TYPE_STRING, PRIMITIVE_TYPE_UUID};

    use super::*;
    use anyhow::{Ok, Result};

    #[test]
    fn test_claim_type() -> Result<()> {
        let mut roles = vec![];
        roles.push(Role {
            role_name: "Persoon".to_string(),
            role_type: RoleType::LabelType(Arc::new(LabelType::new(
                "Persoon",
                PRIMITIVE_TYPE_UUID.clone(),
            ))),
        });
        roles.push(Role {
            role_name: "Gebouw".to_string(),
            role_type: RoleType::LabelType(Arc::new(LabelType::new(
                "Gebouw",
                PRIMITIVE_TYPE_STRING.clone(),
            ))),
        });
        let claim_type_builder = ClaimTypeBuilder::new(
            "claim_type".to_string(),
            "De eigenaar van <Gebouw:string> is het persoon <Persoon:uuid>".to_string(),
            roles,
        );

        // claim_type_builder
        //     .add_object_type_expression("<Persoon:string> bezit <Gebouw:uuid>".to_string());

        let claim_type = claim_type_builder.build();

        let mut claim_type_lock = claim_type.inner.lock().unwrap();
        claim_type_lock.add_unicity(vec!["Persoon".to_string(), "Gebouw".to_string()]);

        println!("{:#?}", claim_type);

        Ok(())
    }
}
