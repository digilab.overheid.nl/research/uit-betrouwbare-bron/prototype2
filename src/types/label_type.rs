use std::{
    fmt::{Display, Formatter},
    sync::Arc,
};

use uuid::Uuid;

use crate::PrimitiveType;

#[derive(Clone, Debug)]
pub struct LabelType {
    pub(crate) inner: Arc<InnerLabelType>,
}

#[derive(Debug)]
pub struct InnerLabelType {
    pub id: Uuid,
    pub name: String,
    pub primitive_type: PrimitiveType,
}

impl LabelType {
    pub fn new<T>(name: T, primitive_type: PrimitiveType) -> Self
    where
        T: Into<String>,
    {
        LabelType {
            inner: Arc::new(InnerLabelType {
                id: Uuid::new_v4(),
                name: name.into(),
                primitive_type,
            }),
        }
    }

    pub(crate) fn inner(&self) -> &Arc<InnerLabelType> {
        &self.inner
    }
}

impl Display for LabelType {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        let inner = self.inner();
        write!(
            f,
            "id: `{}`, name: `{}`, primitive type: `{}`",
            inner.id, inner.name, inner.primitive_type
        )
    }
}
