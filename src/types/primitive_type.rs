use std::{
    fmt::{Display, Formatter},
    sync::Arc,
};

use once_cell::sync::Lazy;

#[derive(Clone, Debug)]
pub struct PrimitiveType {
    pub(crate) inner: Arc<InnerPrimitiveType>,
}

#[derive(Debug)]
pub struct InnerPrimitiveType {
    pub name: String,
    validator: fn(&str) -> bool,
}

impl PrimitiveType {
    pub(crate) fn new<T>(name: T, validator: fn(&str) -> bool) -> Self
    where
        T: Into<String>,
    {
        PrimitiveType {
            inner: Arc::new(InnerPrimitiveType {
                name: name.into(),
                validator,
            }),
        }
    }

    pub fn name(&self) -> &str {
        &self.inner.name
    }

    pub fn validate_value(&self, value: &str) -> bool {
        (self.inner.validator)(value)
    }
}

impl Display for PrimitiveType {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Primitive Type: {}", self.name())
    }
}

pub static PRIMITIVE_TYPE_UUID: Lazy<PrimitiveType> =
    Lazy::new(|| PrimitiveType::new("uuid", |value| uuid::Uuid::parse_str(value).is_ok()));

pub static PRIMITIVE_TYPE_INTEGER: Lazy<PrimitiveType> =
    Lazy::new(|| PrimitiveType::new("integer", |value| value.parse::<i64>().is_ok()));
pub static PRIMITIVE_TYPE_STRING: Lazy<PrimitiveType> =
    Lazy::new(|| PrimitiveType::new("string", |_value| true));
pub static PRIMITIVE_TYPE_BOOLEAN: Lazy<PrimitiveType> =
    Lazy::new(|| PrimitiveType::new("bool", |value| value == "true" || value == "false"));
pub static PRIMITIVE_TYPE_DATE: Lazy<PrimitiveType> = Lazy::new(|| {
    PrimitiveType::new("date", |value| {
        chrono::NaiveDate::parse_from_str(value, "%Y-%m-%d").is_ok()
    })
});
pub static PRIMITIVE_TYPE_DATETIME: Lazy<PrimitiveType> = Lazy::new(|| {
    PrimitiveType::new("datetime", |value| {
        chrono::NaiveDateTime::parse_from_str(value, "%Y-%m-%d %H:%M:%S").is_ok()
    })
});
pub static PRIMITIVE_TYPE_DECIMAL: Lazy<PrimitiveType> =
    Lazy::new(|| PrimitiveType::new("decimal", |value| value.parse::<f64>().is_ok()));

pub fn primitive_type_form_str(name: &str) -> Option<&'static PrimitiveType> {
    match name {
        "uuid" => Some(&PRIMITIVE_TYPE_UUID),
        "integer" => Some(&PRIMITIVE_TYPE_INTEGER),
        "string" => Some(&PRIMITIVE_TYPE_STRING),
        "bool" => Some(&PRIMITIVE_TYPE_BOOLEAN),
        "date" => Some(&PRIMITIVE_TYPE_DATE),
        "datetime" => Some(&PRIMITIVE_TYPE_DATETIME),
        "decimal" => Some(&PRIMITIVE_TYPE_DECIMAL),
        _ => None,
    }
}
